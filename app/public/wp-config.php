<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );
/** MySQL database username */
define( 'DB_USER', 'root' );
/** MySQL database password */
define( 'DB_PASSWORD', 'root' );
/** MySQL hostname */
define( 'DB_HOST', 'localhost' );
/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );
/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i9Z6pvB8WbBqCHWphhHD8BTDZ2Fifemonb5xqdR/wVFvNy7N/gbo94ZkKhFWdYTkgEWysJeWGaE2KcywXyZ0xQ==');
define('SECURE_AUTH_KEY',  'jxgtMy9MdQMjiMY67FSqCpAUu/N9D7LkoiugHdnOMZO1pOyWIQTsfpSs5TsB5W15qLKCCheE/R1Vnzs1ASHFWA==');
define('LOGGED_IN_KEY',    'WRAYyCFIU03e9UGctqUAkLs6hbv2EMhQPB5T+HFG9MlOGC2d2NgiwlaQeF/mb/liFxQzwvfriM2d0uicc/by4g==');
define('NONCE_KEY',        '0GBxzXtTn67rNNDHXky+nqNPrCeXws5OoH2YgbNWT97TEKyRQRxZN4TQpwVDALnSKaa8fY4tBLV+DUc9Eb48Tw==');
define('AUTH_SALT',        'QZ4+uZt1b0WCm+0OLM9bB71GtnDgZ8vIRS2Br80sjK/MgSUWl41XJ0BePbgPmHORHYmvamVFEHBnHTiZ6qcQ9Q==');
define('SECURE_AUTH_SALT', 'bWhpk08si8a1YDk+Lmudxp5BHysI/oaOlMDOsqkyPiFFjrxjoTKMch9nv9iJTC3T1Gp4lq3smUkbnrxQYW+t6g==');
define('LOGGED_IN_SALT',   'hH9hI25519jBLl9cgV4Ww0y5c8V7d4f76x2pgSRBPIxpQqH+2dHl2zZeWTB4sEoRO6NYXBv7xQcqYlg2nvoDRg==');
define('NONCE_SALT',       'spOZSzlS5ExHmpD1rTjkWVaOyNACQQe02blUHFh+zbF2AWW5W/nzJTM54m1uzlPaaWP3+POewq/IQ1x/I50Qvw==');
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_o5myaaat7n_';
/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
