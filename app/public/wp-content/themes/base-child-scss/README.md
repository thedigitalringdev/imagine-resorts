# Base Child Theme Starter with SCSS

---

Use this child theme with the Total Base Theme.

### Features:

- SCSS file structure
- Autoprefixing for browser prefixes
- JS file concatenation
- Minification (needs to be configured)
- Sourcemaps for CSS inspection
- Browsersync for CSS injection and JS reloading 🤤...


### To use:

1. Clone or download from the repo.
2. Change the child theme name in the `base-child/assets/sass/style.scss` file to reflect the project.
3. Make sure you have the gulp CLI installed globally: `npm install gulp-cli -g`.
4. In terminal, `cd` to the child theme directory and run `npm install`.
5. In the `gulpfile.js`, change the Browsersync proxy from `base.test` to whatever your local install domain is.
6. If everything installed correctly, you should be able to run gulp and everything should load, Browsersync should fire up, and be good to go.
7. Enter GULP to setup.


