<?php


/**
 * Enqueue Scripts and Styles
 * @var [type]
 */
add_action( 'wp_enqueue_scripts', 'base_child_theme_enqueue_styles' );

function base_child_theme_enqueue_styles() {

    $my_js_ver  = date("ymd-Gis", time() );

    //Enqueue Parent Styles
    $parent_style = 'wpex-style-css'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );

    //Enqueue Child Styles and Scripts
	// wp_enqueue_style( 'included-styles', get_stylesheet_directory_uri() . '/css/included_styles.css' );
	wp_enqueue_script( 'app-js', get_stylesheet_directory_uri() . '/assets/app.js', array( 'jquery' ), $my_js_ver, false );

}

?>
