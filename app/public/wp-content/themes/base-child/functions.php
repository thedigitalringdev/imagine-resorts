<?php
/**
 * Child theme functions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Text Domain: wpex
 * @link http://codex.wordpress.org/Plugin_API
 *
 */

/**
 * Load the parent style.css file
 *
 * @link http://codex.wordpress.org/Child_Themes
 */
add_action( 'wp_enqueue_scripts', 'total_child_enqueue_parent_theme_style' );
function total_child_enqueue_parent_theme_style() {

	// Dynamically get version number of the parent stylesheet (lets browsers re-cache your stylesheet when you update your theme)
	$theme   = wp_get_theme( 'base' );
	$version = $theme->get( 'Version' );

	// Load the stylesheet
	wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css', array(), $version );
	
}


// Remove notice to install recommended plugins
add_filter( 'wpex_recommended_plugins', '__return_empty_array' );


// Copy parent theme mods on first activation
// Does not include any options the theme may store separately
add_action( 'after_switch_theme', 'support_copy_theme_mods' );

function support_copy_theme_mods() {

    $mods = get_theme_mods();
    
    if ( is_null( $mods ) || ( ( 1 == count( $mods ) ) && ! $mods[0] ) ) {
        $parent = wp_get_theme()->get('Template');
        $child = basename( dirname( __FILE__ ) );
        update_option( "theme_mods_$child", get_option( "theme_mods_$parent" ) );
    }
}
